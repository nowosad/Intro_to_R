# Prerequisites

## R

- https://www.r-project.org/
- http://cran.rstudio.com/bin/linux/
- http://cran.rstudio.com/bin/windows/base/
- http://cran.rstudio.com/bin/macosx/

## RStudio

- https://www.rstudio.com/products/rstudio/download/

## R packages

- `install.packages('ggplot2')`
- `install.packages('gapminder')`

## Datasets

- [gapminder]()
- [skijumps]()
- [temperature]()